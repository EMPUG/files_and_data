import sys
from pathlib import Path


if len(sys.argv) < 2:
    print('please provide choice, friend or world')
    exit()


choice = sys.argv[1]
cwd = Path.cwd()


if choice == 'friend':
    file_name = 'data.dat'
    mode = 'wb'

    local_path = Path(cwd, file_name)

    with open(local_path, mode) as file_object:
        file_object.write(b'Hello Friend')


if choice == 'world':
    file_name = 'hello.txt'
    mode = 'wt' or 'w'  # t is default

    local_path = Path(cwd, file_name)

    with open(local_path, mode) as file_object:
        file_object.write(f'Hello {choice.title()}')
