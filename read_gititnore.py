import sys
from pathlib import Path


if len(sys.argv) > 1:
    directory = Path(sys.argv[1])
    gitignore_path = directory / '.gitignore'
    if gitignore_path.is_file():
        print(gitignore_path.read_text(), end='')
else:
    print('please supply directory name ("." for cwd)')
