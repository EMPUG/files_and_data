import sys
from pathlib import Path


def make_config(dir_path):
    """Create .config file in given directory and return filepath."""
    path = Path(dir_path, '.config')
    if not path.exists():
        path.parent.mkdir(exist_ok=True, parents=True)
        path.touch()
        msg = 'was created'
    else:
        msg = 'already exists'
    return path, msg


if __name__ == '__main__':
    if len(sys.argv) > 1:
        folder_name = sys.argv[1]
        result_path, debug = make_config(folder_name)
        print(result_path, debug)
    else:
        print('please supply directory name ("." for cwd)')
