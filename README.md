# "files and data"
_great title_

This repo is a collection of notes and examples prepared for the EMPUG meetup on Sept 19th 2020.  

This could use a lot of improvements!! :D    

There are three scripts, two of which are from the examples in this readme.  
`hello.py`  
`read_gitignore.py`  
`make_config.py`  
When ran without, they indicate they are expecting an argument.  

The examples below are adapted from the articles at the links available below.    
(from treyhunner.com)  

Oh yeah there is also a blender file `untitled.blend` that has a script that reads vector coordinates from the `points.csv` file here as an abstract example.  

I would also like to add there are no 3rd party dependencies for these scripts, no virtual environment or requirements installation is required.  

At the moment I intend to continue to keep this dependency free, and purely about Python itself.  

# Reading the Filesystem
_is there a directory or file here?_




## Two primary options



### `os.path`

`os.path.abspath`  
`os.path.exists`  
`os.path.isfile`  
`os.path.isdir`  
`os.path.join`  



```python
import os


def make_config(dir_path):
    """Create .config file in given directory and return filename."""
    filename = os.path.join(dir_path, '.config')
    if not os.path.exists(filename):
        os.makedirs(dir_path, exist_ok=True)
        open(filename, mode='wt').write('')
    return filename
```


```python
import os.path
import sys


directory = sys.argv[1]
ignore_filename = os.path.join(directory, '.gitignore')
if os.path.isfile(ignore_filename):
    with open(ignore_filename, mode='rt') as ignore_file:
        print(ignore_file.read(), end='')
```




## `pathlib`
https://treyhunner.com/2018/12/why-you-should-be-using-pathlib/   
https://treyhunner.com/2019/01/no-really-pathlib-is-great/


```python
from pathlib import Path


def make_config(dir_path):
    """Create .config file in given directory and return filepath."""
    path = Path(dir_path, '.config')
    if not path.exists():
        path.parent.mkdir(exist_ok=True, parents=True)
        path.touch()
    return path
```

```python
from pathlib import Path
import sys


directory = Path(sys.argv[1])
ignore_path = directory / '.gitignore'
if ignore_path.is_file():
    print(ignore_path.read_text(), end='')
```

### notes
As of 3.6 the `open` function and most relevant `os.path` functions also accept a path-like-object.

The `pathlib` module may not be as performant with large collections of files.

The `/` separators in `pathlib.Path` strings are automatically converted to the correct path separator based on the operating system you’re on. This is a huge feature that can make for code that is more readable and more certain to be free of path-related bugs.

#### glob

	
```python
from glob import glob


top_level_csv_files = glob('*.csv')
all_csv_files = glob('**/*.csv', recursive=True)
```

```python
from pathlib import Path

top_level_csv_files = Path.cwd().glob('*.csv')
all_csv_files = Path.cwd().rglob('*.csv')

```

# read and writing to a file

## `open`
https://docs.python.org/3.6/library/functions.html#open

Take a path like object and return the corresponding file object.

from the docs:
> The type of file object returned by the open() function depends on the mode. When open() is used to open a file in a text mode ('w', 'r', 'wt', 'rt', etc.), it returns a subclass of io.TextIOBase (specifically io.TextIOWrapper). When used to open a file in a binary mode with buffering, the returned class is a subclass of io.BufferedIOBase. The exact class varies: in read binary mode, it returns an io.BufferedReader; in write binary and append binary modes, it returns an io.BufferedWriter, and in read/write mode, it returns an io.BufferedRandom. When buffering is disabled, the raw stream, a subclass of io.RawIOBase, io.FileIO, is returned.

| Character | Meaning |
|---|---|
| 'r' | open for reading (default) |
| 'w' | open for writing, truncating the file first |
| 'x' | open for exclusive creation, failing if the file already exists |
| 'a' | open for writing, appending to the end of the file if it exists |
| 'b' | binary mode |
| 't' | text mode (default) |
